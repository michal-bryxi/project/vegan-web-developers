# Vegan Web Developers

⚠️ The project is up and running at [j9t.gitlab.io/vegan-web-developers](https://j9t.gitlab.io/vegan-web-developers/) but still being set up.

This is a list of [vegan](https://en.wikipedia.org/wiki/Veganism) web developers. Every vegan web developer may join: Add yourself directly [in the HTML](https://gitlab.com/j9t/vegan-web-developers/-/blob/master/index.html) and [submit a merge request](https://gitlab.com/j9t/vegan-web-developers/-/merge_requests/new). Please sort by last name, link only personal websites and Twitter accounts, and stick with the existing structure, including no use of [optional markup](https://meiert.com/en/blog/optional-html/). (Alternatively, [ask to be added](https://gitlab.com/j9t/vegan-web-developers/-/issues/new).)

The list is based on an honor system. If you’re not a vegan and not a web developer, don’t join, or take yourself off the list again once you stopped being vegan or a web developer.